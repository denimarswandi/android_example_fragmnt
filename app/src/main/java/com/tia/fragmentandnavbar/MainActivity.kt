package com.tia.fragmentandnavbar

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.text.InputFilter
import android.view.View
import android.widget.Button
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        loadHome(HomeFragment())

        val btn = findViewById<Button>(R.id.button1)

        btn.setOnClickListener({
            val intent = Intent(this@MainActivity, newActivity::class.java)
            var hello: String = "Hello Activity baru"
            intent.putExtra("hello", hello)
            startActivity(intent)
        })

        val navbar = findViewById<View>(R.id.navbar) as BottomNavigationView
        navbar.setOnNavigationItemSelectedListener { item ->
            when(item.itemId){
                R.id.home ->{
                    Toast.makeText(application, "Home", Toast.LENGTH_LONG).show()
                    loadHome(HomeFragment())
                }
                R.id.info -> {
                    Toast.makeText(application, "Info", Toast.LENGTH_LONG).show()
                    loadInfo(InfoFragment())
                }
            }
            true
        }
    }

    private fun loadHome(home: HomeFragment){
        val ho = supportFragmentManager.beginTransaction()
        ho.replace(R.id.fragment, home)
        ho.commit()
    }

    private  fun loadInfo(info: InfoFragment){
        val fo = supportFragmentManager.beginTransaction()
        fo.replace(R.id.fragment, info)
        fo.commit()
    }
}
