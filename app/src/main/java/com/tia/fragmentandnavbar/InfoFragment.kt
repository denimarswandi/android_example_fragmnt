package com.tia.fragmentandnavbar

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_info.view.*


class InfoFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val fragment = LayoutInflater.from(container?.context).inflate(R.layout.fragment_info, container,false)
        val btn = fragment.findViewById<Button>(R.id.button1)
        btn.setOnClickListener {
            Toast.makeText(activity, "Info", Toast.LENGTH_LONG).show()
        }
        return fragment
    }
}