package com.tia.fragmentandnavbar

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.*


class HomeFragment : Fragment() {



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val fragment =  LayoutInflater.from(container?.context).inflate(R.layout.fragment_home, container, false)

        val btn = fragment.findViewById<Button>(R.id.button)

        btn.setOnClickListener({
            Toast.makeText(activity, "Ia from fragmaent", Toast.LENGTH_LONG).show()
        })

        return fragment
    }
}